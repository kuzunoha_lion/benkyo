'''
Python
https://qiita.com/jnchito/items/f07e58824f92395c353b
https://www.youtube.com/watch?v=HsivL1JBRaw
RubyのこれをPythonに移植してみよう！
'''
import datetime
import unittest


class Taiyaki:
    def __init__(self, anko, size):
        self.anko = anko
        self.size = size
        self.produced_on = datetime.date.today()

    def to_s(self):
        return 'あんこ: {anko}, 大きさ: {size}'.format(anko=self.anko, size=self.size)

    def price(self):
        amount = 100
        if self.anko == '白あん':
            amount += 30
        if self.size == '大きい':
            amount += 50
        return amount

    def expire_on(self):
        out = datetime.timedelta(days=3)
        return self.produced_on + out

    def is_expired(self, today=datetime.date.today()):
        return self.expire_on() < today


class TaiyakiTest(unittest.TestCase):

    def test_taiyaki(self):
        taiyaki_1 = Taiyaki('あんこ', 'ふつう')
        self.assertEqual('あんこ', taiyaki_1.anko)
        self.assertEqual('ふつう', taiyaki_1.size)
        self.assertEqual('あんこ: あんこ, 大きさ: ふつう', taiyaki_1.to_s())
        self.assertEqual(100, taiyaki_1.price())

        taiyaki_2 = Taiyaki('白あん', '大きい')
        self.assertEqual('白あん', taiyaki_2.anko)
        self.assertEqual('大きい', taiyaki_2.size)
        self.assertEqual('あんこ: 白あん, 大きさ: 大きい', taiyaki_2.to_s())
        self.assertEqual(180, taiyaki_2.price())

    def test_expir(self):
        taiyaki = Taiyaki('あんこ', 'ふつう')
        # 製造日 = システム日付
        self.assertEqual(datetime.date.today(), taiyaki.produced_on)
        # 賞味期限 = 製造日 + 3日
        self.assertEqual((datetime.date.today() +
                          datetime.timedelta(days=3)), taiyaki.expire_on())

        # 当日なら食べられる
        self.assertFalse(taiyaki.is_expired())

        # 3日目まで食べられる
        self.assertFalse(taiyaki.is_expired(
            datetime.date.today() + datetime.timedelta(days=3)))

        # 4日目を過ぎると食べられない
        self.assertTrue(taiyaki.is_expired(
            datetime.date.today() + datetime.timedelta(days=4)))


if __name__ == '__main__':
    unittest.main(exit=False)
