import os
import subprocess


def run_openssl(data):
    env = os.environ.copy()
    env['sandbox'] = b'\\xe24U\\n\\xdo013S\\x11'
    proc = subprocess.Popen(
        ['openssl', 'enc', '-des3', '-pass', 'env:sandbox'],
        env=env,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE)
    proc.stdin.write(data)
    proc.stdin.flush()
    return proc


input_procs = []
hash_procs = []
for _ in range(3):
    data = os.urandom(10)
    proc = run_openssl(data)
    input_procs.append(proc)
    hash_procs = run_md5(proc)

for proc in procs:
    out, err = proc.communicate()
    print(out[-10:])
