class Oyaclass(object):
    def __init__(self):
        self.number = 3

    def output(self):
        return self.number
 

class Koclass(Oyaclass):
    def plusone(self):
        self.number += 1
        return self.number


oya = Oyaclass()
print(oya.output())
ko = Koclass()
print(ko.output())
print(ko.plusone())
print(ko.number)
print(oya.number)
