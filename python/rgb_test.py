'''
RGBの値を変換する関数を格納しています。
'''
import re
import unittest


def to_hex(red, green, blue):
    '''
    10進数→16進数

    to_hex(int,int,int)で渡してください。
    '#str'を返り値にします。

    to_hex(0,0,0) => '#000000'
    to_hex(255,255,255) => '#ffffff'
    to_hex(4,60,120) => '#043c78'
    '''

    order = '#'
    for i in (red, green, blue):
        order += hex(i)[2:4].zfill(2)
    return order


def to_ints(ihex):
    '''
    16進数→10進数

    to_ints(str)で渡してください。
    初めに'#'をつけてください。
    array[int,int,int]を返り値にします。

    to_ints('#000000') => [0, 0, 0]
    to_ints('#ffffff') => [255, 255, 255]
    to_ints('#043c78') => [4, 60, 120]
    '''
    order = []
    pattern = r"\w\w"
    order = list(map(lambda x: int(x, 16), re.findall(pattern, ihex)))
    return order


class RGBtest(unittest.TestCase):
    '''
    単体テスト
    '''

    def test_to_hex(self):
        '''
        to_hex関数のテスト
        to_hex(0,0,0) => #000000
        to_hex(255,255,255) => #ffffff
        to_hex(4,60,120) => #043c78
        '''
        self.assertEqual('#000000', to_hex(0, 0, 0))
        self.assertEqual('#ffffff', to_hex(255, 255, 255))
        self.assertEqual('#043c78', to_hex(4, 60, 120))

    def test_to_ints(self):
        '''
        to_ints関数のテスト
        to_ints('#000000') => [0, 0, 0]
        to_ints('#ffffff') => [255, 255, 255]
        to_ints('#043c78') => [4, 60, 120]
        '''
        self.assertEqual([0, 0, 0], to_ints('#000000'))
        self.assertEqual([255, 255, 255], to_ints('#ffffff'))
        self.assertEqual([4, 60, 120], to_ints('#043c78'))


if __name__ == '__main__':
    unittest.main(exit=False)
