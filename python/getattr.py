class LazyDB(object):
    def __init__(self):
        self.exists = 5

    def __getattr__(self, name):
        value = 'Value of %s' % name
        setattr(self, name, value)
        return value


class LoggingLazyDB(LazyDB):
    def __getattr__(self, name):
        print('Called __getattribute__(%s)' % name)
        return super().__getattr__(name)


data = LoggingLazyDB()
print('Before:', data.__dict__)
print('foo:', data.foo)
print('foo:', data.foo)
print('After:', data.__dict__)
