'''
Ruby
https://qiita.com/jnchito/items/f07e58824f92395c353b
https://www.youtube.com/watch?v=HsivL1JBRaw
これをPython→Rubyでやってみよう
'''
require 'date'
require 'minitest/autorun'


class Taiyaki
    attr_reader :anko, :size, :produced_on

    def initialize(anko, size)
        @anko = anko
        @size = size
        @produced_on = Date.today
    end

    def to_s
        "あんこ: #{anko}, 大きさ: #{size}"
    end

    def price
        amount = 100
        if anko == '白あん'
            amount += 30
        end
        if size == '大きい'
            amount += 50
        end
        amount
    end

    def expire_on
        produced_on + 3
    end

    def expired?(today = Date.today)
        expire_on < today
    end

end

class TaiyakiTest < Minitest::Test
    
    def test_taiyaki
        taiyaki_1 = Taiyaki.new('あんこ', 'ふつう')
        assert_equal 'あんこ', taiyaki_1.anko
        assert_equal 'ふつう', taiyaki_1.size
        assert_equal 'あんこ: あんこ, 大きさ: ふつう', taiyaki_1.to_s
        assert_equal 100, taiyaki_1.price
        
        taiyaki_2 = Taiyaki.new('白あん', '大きい')
        assert_equal '白あん', taiyaki_2.anko
        assert_equal '大きい', taiyaki_2.size
        assert_equal 'あんこ: 白あん, 大きさ: 大きい', taiyaki_2.to_s
        assert_equal 180, taiyaki_2.price
    end
    def test_expir
        taiyaki = Taiyaki.new('あんこ', 'ふつう')
        assert_equal Date.today, taiyaki.produced_on
        assert_equal (Date.today + 3), taiyaki.expire_on
        refute taiyaki.expired?
        refute taiyaki.expired?(Date.today + 3)
        assert taiyaki.expired?(Date.today + 4)
    end
end
